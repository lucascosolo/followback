const Twit = require('twit');
const config = require('./config.json');

var bot = new Twit(config);

var time = 0;

var ifbStream = bot.stream('statuses/filter', { track: 'ifb,#followback', language: 'en' });
ifbStream.on('tweet', function (tweet) {
  if (tweet.user.screen_name.toLowerCase() === 'linkupus') return;
  var between = Date.now() - time;
  if (between < 1000 * 60 * 15) return;
  time = Date.now();
  bot.post('friendships/create', { user_id: tweet.user.id_str }, function (err, data, res) {
    if (err) console.error(err);
    else {
      console.log('Followed @' + tweet.user.screen_name);
      var mentions = '@' + tweet.user.screen_name + ' ';
      for (var i in tweet.entities.user_mentions) {
        var mention = tweet.entities.user_mentions[i];
        if (mentions.indexOf(mention.screen_name) == -1) {
          mentions = mentions + '@' + mention.screen_name + ' ';
          bot.post('friendships/create', { user_id: tweet.user.id_str }, function (err, data, res) {});
        }
      }
      bot.post('statuses/update', { status: mentions + 'I will follow, please #followback! IFB', in_reply_to_status_id: tweet.id_str }, function (err, data, res) {
        if (err) console.error(err);
        else {
          console.log('Posted reply.');
          bot.post('favorites/create', { id: tweet.id_str }, function (err, data, res) {
            if (err) console.error(err);
            else {
              console.log('Liked tweet.');
            }
          });
        }
      });
    }
  });
});

function followBack(ids, idx) {
  bot.post('friendships/create', { user_id: ids[idx] }, function (err, data, res) {
    if (err) console.error(err);
    else {
      console.log('Followed back ID ' + ids[idx]);
      setTimeout(function () {
        followBack(ids, idx+1);
      }, 1000 * 60 * 15);
    }
  });
}

function findFollowers() {
  bot.get('followers/ids', { count: 200, stringify_ids: true }, function (err, data, res) {
    if (err) console.error(err);
    else {
      followBack(data.ids, 0);
    }
  });
  setTimeout(findFollowers, 1000 * 60 * 60 * 24);
}

findFollowers();